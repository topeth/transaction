package com.rabotest.transaction.model;

public enum TransactionType {
    OK("Successful"),
    NOK("Failed");

    private final String value;
    /**
     * @param value
     */
    TransactionType(final String value) {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return value;
    }
    public String getValue() {
        return value;
    }
}
