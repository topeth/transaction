package com.rabotest.transaction.model;

public enum SourceType {
    CSV("csv"),
    XML("xml");

    private final String value;
    /**
     * @param value
     */
    SourceType(final String value) {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return value;
    }
    public String getValue() {
        return value;
    }
}
