package com.rabotest.transaction.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@Table(name = "csv_report")
public class ReportEntity {
    @Id
    @GeneratedValue
    private long id;

    private int reference;
    private String accountNumber;
    private String description;
    private double startBalance;
    private double mutation;
    private double endBalance;
    private String status;
    private String remarks;
    private String source;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReportEntity)) return false;
        ReportEntity ReportEntity = (ReportEntity) o;
        return getId() == ReportEntity.getId();
    }
    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
