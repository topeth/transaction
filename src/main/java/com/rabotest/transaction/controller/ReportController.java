package com.rabotest.transaction.controller;

import com.rabotest.transaction.service.ReportService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
@AllArgsConstructor
@CrossOrigin("http://localhost:4200")
public class ReportController {

    ReportService reportService;

    @GetMapping("/report")
    public Mono<ResponseEntity> getReport() {
        return Mono.just(reportService.getReport())
                .map(report -> ResponseEntity.status(HttpStatus.OK).body(report))
                .cast(ResponseEntity.class)
                .defaultIfEmpty(ResponseEntity.status(HttpStatus.NO_CONTENT).body("Nothing found"));
    }

    @GetMapping("/successreport")
    public Mono<ResponseEntity> getSuccessReport() {
        return Mono.just(reportService.getSuccessReport())
                .map(report -> ResponseEntity.status(HttpStatus.OK).body(report))
                .cast(ResponseEntity.class)
                .defaultIfEmpty(ResponseEntity.status(HttpStatus.NO_CONTENT).body("Nothing found"));
    }

    @GetMapping("/failurereport")
    public Mono<ResponseEntity> getFailureReport() {
        return Mono.just(reportService.getFailureReport())
                .map(report -> ResponseEntity.status(HttpStatus.OK).body(report))
                .cast(ResponseEntity.class)
                .defaultIfEmpty(ResponseEntity.status(HttpStatus.NO_CONTENT).body("Nothing found"));
    }
}

