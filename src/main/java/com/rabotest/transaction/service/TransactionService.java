package com.rabotest.transaction.service;

import com.rabotest.transaction.entity.ReportEntity;
import com.rabotest.transaction.repository.ReportRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class TransactionService {

    private ReportRepository reportRepository;

    public List<ReportEntity> getAllTransactions() {
        return reportRepository.findAll();
    }

    public List<ReportEntity> getAllTransactionsByStatus(String status) {
        return reportRepository.findByStatus(status);
    }

    public void addTransaction(ReportEntity ReportEntity) {
        reportRepository.save(ReportEntity);
    }
}
