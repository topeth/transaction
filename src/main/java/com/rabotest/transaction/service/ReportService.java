package com.rabotest.transaction.service;

import com.rabotest.transaction.entity.ReportEntity;
import com.rabotest.transaction.model.SourceType;
import com.rabotest.transaction.handler.TransactionHandler;
import com.rabotest.transaction.model.Transaction;
import com.rabotest.transaction.model.TransactionType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.rabotest.transaction.model.SourceType.*;
import static com.rabotest.transaction.model.TransactionType.*;

@Service
@Slf4j
@AllArgsConstructor
public class ReportService {

    TransactionService transactionService;
    TransactionHandler transactionHandler;

    private static final String REFERENCE_ERR_MSG = "Reference Error";
    private static final String BALANCE_ERR_MSG = "Balance Error";
    private static final String SUCCESS_MSG = "Transaction OK";

    @EventListener(ApplicationReadyEvent.class)
    public void createReport() {
        List<Integer> processedReferences = new ArrayList<>();
        populateDataFromCSVSource(processedReferences);
        populateDataFromXMLSource(processedReferences);
    }

    private void populateDataFromCSVSource(List<Integer> processedReferences) {
        List<Transaction> transactions = transactionHandler.getTransactionsFromCsv();

        transactions.forEach(transaction -> {
            if(processedReferences.contains(transaction.getReference())){
                saveToDb(CSV, transaction, NOK, REFERENCE_ERR_MSG);
            }else if(isBalanceNotOk(transaction)){
                saveToDb(CSV, transaction, NOK, BALANCE_ERR_MSG);
            }else{
                saveToDb(CSV, transaction, OK, SUCCESS_MSG);
            }
            processedReferences.add(transaction.getReference());
        });
    }

    private void populateDataFromXMLSource(List<Integer> processedReferences) {
        List<Transaction> transactions = null;
        try {
            transactions = transactionHandler.getTranscationsFromXML();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        Objects.requireNonNull(transactions).forEach(transaction -> {
            if(processedReferences.contains(transaction.getReference())){
                saveToDb(XML, transaction, NOK, REFERENCE_ERR_MSG);
            }else if(isBalanceNotOk(transaction)){
                saveToDb(XML, transaction, NOK, BALANCE_ERR_MSG);
            }else{
                saveToDb(XML, transaction, OK, SUCCESS_MSG);
            }
            processedReferences.add(transaction.getReference());
        });
    }

    private boolean isBalanceNotOk(Transaction transaction){
        BigDecimal balanceAfterMutation = BigDecimal.valueOf(transaction.getStartBalance())
                .add(BigDecimal.valueOf(transaction.getMutation()));
        BigDecimal endBalance = BigDecimal.valueOf(transaction.getEndBalance());
        return balanceAfterMutation.compareTo(endBalance) != 0;
    }

    private void saveToDb(SourceType sType, Transaction transaction, TransactionType tType, String remarks){
        ReportEntity reportEntity = new ReportEntity();
        reportEntity.setReference(transaction.getReference());
        reportEntity.setDescription(transaction.getDescription());
        reportEntity.setAccountNumber(transaction.getAccountNumber());
        reportEntity.setStartBalance(transaction.getStartBalance());
        reportEntity.setMutation(transaction.getMutation());
        reportEntity.setEndBalance(transaction.getEndBalance());
        reportEntity.setStatus(tType.getValue());
        reportEntity.setRemarks(remarks);
        reportEntity.setSource(sType.getValue());
        transactionService.addTransaction(reportEntity);
    }

    public List<ReportEntity> getReport() {
         return transactionService.getAllTransactions();
    }
    public List<ReportEntity> getSuccessReport() {
        return transactionService.getAllTransactionsByStatus(OK.getValue());
    }
    public List<ReportEntity> getFailureReport() {
        return transactionService.getAllTransactionsByStatus(NOK.getValue());
    }
}
