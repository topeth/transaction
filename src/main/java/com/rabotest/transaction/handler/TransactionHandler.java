package com.rabotest.transaction.handler;

import com.rabotest.transaction.model.Transaction;
import com.rabotest.transaction.model.Transactions;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class TransactionHandler {

    public List<Transaction> getTransactionsFromCsv() {
        List<Transaction> transactions = new ArrayList<>();
        Path pathToFile = null;
        try {
            pathToFile = Paths.get(ClassLoader.getSystemResource("transactions/records.csv").toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = Files.newBufferedReader(Objects.requireNonNull(pathToFile),
                StandardCharsets.UTF_8)) {
            br.readLine();
            String line;

            while ((line = br.readLine()) != null) {
                String[] attributes = line.split(",");
                Transaction transaction = createTransaction(attributes);
                transactions.add(transaction);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return transactions;
    }

    private Transaction createTransaction(String[] metadata) {
        int reference = Integer.parseInt(metadata[0]);
        String accountNumber = metadata[1];
        String description = metadata[2];
        double startBalance = Double.parseDouble(metadata[3]);
        double mutation = Double.parseDouble(metadata[4]);
        double endBalance = Double.parseDouble(metadata[5]);

        // create and return transaction of this metadata
        Transaction transaction = new Transaction();
        transaction.setReference(reference);
        transaction.setAccountNumber(accountNumber);
        transaction.setDescription(description);
        transaction.setStartBalance(startBalance);
        transaction.setMutation(mutation);
        transaction.setEndBalance(endBalance);

        return transaction;
    }

    public List<Transaction> getTranscationsFromXML() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Transactions.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        String fileName = "transactions/records.xml";
        File file = new File(Objects.requireNonNull(ClassLoader.getSystemResource(fileName)).getFile());

        Transactions transactions = (Transactions) jaxbUnmarshaller.unmarshal(file);

        return new ArrayList<>(transactions.getTransactions());
    }
}
